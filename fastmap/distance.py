"""Distance metrics for syscalls and syscall sequences
"""

import numpy as np
from cyactive.sysparse import Syscall

# Syscall sequence detection algorithms rely on proximity
# measures/distance metrics between sequences. This module
# builds an hierarchy of distance computations, from distances
# between individual system calls tyo inter-sequence distances.


# Levenshtein distance (wikipedia:Levenshtein_distance), a
# variant of edit distance, is a simple way to measure distance
# between sequences (strings). System calls include strings for
# representation of argument values, and short of defining call
# and argument specific distance metrics, edit distance seems to
# be the only viable solution.

def d_lev(a, b):
    """Computes Levenshtein's distance between lists a and b.
    """
    # Dynamic programming copied verbatim from Wikipedia
    m, n = len(a), len(b)
    d = np.zeros((m+1,n+1))

    for i in range(1, m+1): d[i,0] = i
    for j in range(1, n+1): d[0,j] = j

    for j in range(n):
        for i in range(m):
            if a[i]==b[j]:
                d[i+1,j+1] = d[i,j]
            else:
                d[i+1,j+1] = min(d[i,j+1], d[i+1,j], d[i,j]) + 1

    return d[m,n]


# System calls (see `cyactive.sysparse.Syscall') are represented
# by their direction (entry/exit), name, and arguments. If
# either the direction or the name do not match, the system
# calls are `different' and uncomparable. As such they get the
# maximum distance of 1. If both the name and the direction are
# the same, the system call arguments are compared, under the
# assumption that they have the same arguments in the same
# order (this is, or should be, ensured by sysdig). 
#
# We compute the distance as half the sum of Levenshtein
# distances of argument values divided by the sum of greater
# lengths. This ensures that the distance between system calls
# that differ only in arguments is at most 0.5.
#
# In addition, an empty argument value serves as a wildcard:
# any value matches an empty value exactly. While this only
# makes sense when the value is empty in the trace, not in
# the window, for simplicity the handling is symmetric.

def d_call(a, b):
    """Computes distance between syscalls a and b.
    Returns the distance between system calls.
    """
    if a.direction==b.direction and a.name==b.name:
        # Argument names and order are the same, compare values.
        d, l = 0., 0.
        for (_, va), (_, vb) in zip(a.args, b.args):
            # Empty values in traces are used as wildcards:
            # any value matches an empty value.
            if va and vb:
                d+= d_lev(va, vb)
                l+= max(len(va), len(vb))
        if l>0:
            # The distance is at most 0.5.
            return 0.5*d/l
        else:
            # All arguments are empty, and thus equal.
            return 0.
    else:
        # System calls are uncomparable, the distance is 1.
        return 1.


# One way to compare system call traces is the distance between
# the call sequence and the best subsequence (BSS) in the
# window. The subsequence is obtained by removing some elements
# from the window. Although examples where BSS does not fairly
# reflect the similarity can be easily built (e.g. swapping of
# independent subsequences --- the distance between "1abxy2" and
# "1xyab2" should be 0 if xy is independent of ab), this measure
# is useful as the base level for comparison.

_INFINITY = float('infinity')

def d_bss(d_elem):
    """Returns function computing BSS distance.
    Arguments:
    d_elem - distance function between sequence elements.
    """

    def _d_bss(seq, win):
        """Computes BSS distance between sequence and window.
        Arguments:
        seq -- sequence (list of syscalls),
        win -- window (list of syscalls),
        Returns the distance.
        """
        # Dynamic programming:
        # d is distance matrix by indices of last consumed
        # element in seq and win.
        n, m = len(seq), len(win)
        d = np.zeros((n+1, m+1), dtype=float)

        # Set to infinity all combinations of indexes where the
        # sequence must either start before the window or end
        # after the window.
        for i in range(n+1):
            q, r = i, m-n+i+1
            for j in range(0, m+1):
                if j < q or j >= r:
                    d[i, j] = _INFINITY

        # Then compute distances recursively as the minimum
        # between matching the current symbol in the sequence
        # either now or earlier.
        for i in range(n):
            q, r = i, m-n+i+1
            for j in range(q, r):
                cost = d_elem(seq[i], win[j])
                d[i+1, j+1] = min(d[i, j] + cost, d[i+1, j])

        # Return the distance when both sequences are consumed.
        return d[n, m]

    return _d_bss

d_trace = d_bss(d_call)

from .fastmap import *
from .distance import d_lev
import pylab
import numpy as np


def buildmap():
    leaders = [ "King Crimson", 
                "King Lear", 
                "Denis Leary", 
                "George Bush",
                "George W. Bush",
                "Barack Hussein Obama",
                "Saddam Hussein",
                "George Leary",
              ]
    return Map(leaders, lambda x, y: d_lev(x,y)/max(len(x), len(y)), 2)

def showmap(map_):
    coords = map_.coords
    pylab.scatter([x[0] for x in coords], [x[1] for x in coords], c="r")
    for i,s in enumerate(map_.points):
        pylab.annotate(s,coords[i])

    pylab.title("Levenshtein distance mapped to 2D coordinates")
    # pylab.show()

"""Unit tests for distance metrics"""

import unittest
from cyactive.sysparse import Syscall
from .distance import *


class TestDistance(unittest.TestCase):
    """Test case for distance"""

    def test_lev(self):
        """Levenshtein distance"""
        self.assertEqual(d_lev("abc", "abc"), 0.)
        self.assertEqual(d_lev("abc", "abd"), 1.)
        self.assertEqual(d_lev("ab", "abd"), 1.)
        self.assertEqual(d_lev("db", "abd"), 2.)

    def test_call(self):
        """Distance between calls"""
        a = Syscall("1 > recv fd=1 size=2048")
        aa = Syscall("1 > recv fd=1 size=2048")
        b = Syscall("1 > recv fd=1 size=1024")
        c = Syscall("1 < recv res=-1")
        d = Syscall("1 > send fd=1 size=2048")
        self.assertEqual(d_call(a,a), 0., "identical are equal")
        self.assertEqual(d_call(a,aa), 0., "same are equal")
        self.assertLessEqual(d_call(a,b), 0.5, "different arguments")
        self.assertEqual(d_call(b,c), 1., "different direction")
        self.assertEqual(d_call(b,d), 1., "different name")

    def test_call_edges(self):
        """Distance between calls, edge cases"""
        a = Syscall("1 < write fd=1 data=a=b")
        b = Syscall("1 < write fd=1 data=ab")
        self.assertGreater(d_call(a, b), 0., "= in data")
        a = Syscall("1 > exit")
        b = Syscall("2 > exit")
        self.assertEqual(d_call(a, b), 0., "no arguments")
        a = Syscall("1 < write fd=1 data=x")
        b = Syscall("2 < write fd=2 data=")
        self.assertEqual(d_call(a, b), 0.5, "an empty argument")
        a = Syscall("1 < write fd= data=x")
        b = Syscall("2 < write fd=2 data=")
        self.assertEqual(d_call(a, b), 0.0, "all arguments empty")

    def test_bss(self):
        """Best-subsequence distance"""
        def d_elem(a,b): return abs(ord(a)-ord(b))
        dist = d_bss(d_elem)
        self.assertEqual(dist("ab", "ab"), 0, "same are equal")
        self.assertEqual(dist("ab", "xaybz"), 0, "interspersed")
        self.assertEqual(dist("ab", "acd"), 1, "first better")
        self.assertEqual(dist("ab", "adc"), 1, "last better")

    def test_bss_edges(self):
        """Best-subsequence distance, edge cases"""
        def d_elem(a,b): return abs(ord(a)-ord(b))
        dist = d_bss(d_elem)
        self.assertEqual(dist("abc", "ab"), float('infinity'), "|seq| > |win|")


def suite():
    """returns testsuite with tests for distances"""
    return unittest.TestSuite([TestDistance(test) 
                               for test in [ "test_lev",
                                             "test_call",
                                             "test_call_edges",
                                             "test_bss",
                                             "test_bss_edges"
                                           ]])

#!/usr/bin/env python

# Taken from http://gromgull.net/blog/2009/08/fastmap-in-python/
# and heavily modified.

# This implements the FastMap algorithm for mapping points where
# only the distance between them is known to N-dimension
# coordinates.  The FastMap algorithm was published in:
#
# FastMap: a fast algorithm for indexing, data-mining and
# visualization of traditional and multimedia datasets by
# Christos Faloutsos and King-Ip Lin
# http://portal.acm.org/citation.cfm?id=223812

# This code made available under the BSD license, details at the
# bottom of the file Copyright (c) 2009, Gunnar Aastrand Grimnes

import math
import random
import numpy as np

class Projection(object):
    """Fastmap projection."""

    def x_p(self, x, k):
        """Projects x onto the k-th axis defined by pa and pb.
        Returns projected coordinate (distance from pa).
        """
        pa, pb = self.pivots[k]
        d2ab=self.dist2_p(pa, pb, k)
        if d2ab <= 0.:
            return 0.
        d2xa=self.dist2_p(x, pa, k)
        d2xb=self.dist2_p(x, pb, k)
        return (d2xa + d2ab - d2xb)/(2*math.sqrt(d2ab))

    def dist2_p(self, x, y, k): 
        """Computes squared distance between x and y
        projected on k-th dimension.
        """
        # Dynamic programming: coordinates for l<k must have
        # been computed.
        d2 = self.dist_o(x, y)**2 - \
               sum((self.coords_p(x)[l] - self.coords_p(y)[l])**2
                    for l in range(k))
        return d2

    def __getattr__(self, name):
        """Reports error on unimplemented abstract attributes."""
        if name in {'pivots', 'dist_o', 'coords_p'}:
            raise NotImplementedError(name)

class Map(Projection): 
    """Distance-preserving mapping into low-dimensional space."""

    def __init__(self, points, dist, K): 
        self.points = points
        self.dist = dist
        self.coords = np.zeros((len(self.points), K), float)
        self.pivots = np.zeros((K, 2), int)

        # Memoize computation of distances.
        dtab = {}
        def dist_o(x, y):
            if x > y:
                x, y = y, x
            key = len(self.points)*x + y
            if key not in dtab:
                dtab[key] = dist(points[x], points[y])
            return dtab[key]
        self.dist_o = dist_o

        # Build the map, the point coordinates are in
        # self.coords.
        self.__build_map(K)

    def coords_p(self, x): 
        """Returns projected coordinate vector for point x."""
        return self.coords[x]

    def __build_map(self, K):
        """Builds the map."""
        # x_p relies on increasing k.
        for k in range(K):
            self.pivots[k] = self.__pickPivot(k)
            for x in range(len(self.points)):
                self.coords[x, k] = self.x_p(x, k)

    def __pickPivot(self, k):
        """Finds two distant points."""
        o1 = random.randint(0, len(self.points) - 1)
        o2 = self.__furthest(o1, k)
        o1 = self.__furthest(o2, k)
        return o1, o2

    def __furthest(self, o, k): 
        """Finds the most distant point from o"""
        d_max = 0
        i_max = None
        for i in range(len(self.points)): 
            d = self.dist2_p(i, o, k)
            if d >= d_max: 
                d_max = d
                i_max = i
        return i_max

class _Point(object):
    """A point for projecting to fastmap space."""
    def __init__(self, value, coords):
        self.value = value
        self.coords = coords

class Embedding(Projection):
    """Embedding into the mapping space."""

    def __init__(self, map_):
        self.pivots = \
                np.array([[_Point(map_.points[pa], map_.coords[pa]),
                           _Point(map_.points[pb], map_.coords[pb])]
                       for pa, pb in map_.pivots])
        def dist_o(x, y):
            return map_.dist(x.value, y.value)
        self.dist_o = dist_o

    def coords_p(self, x):
        return x.coords

    def project(self, point):
        """Projects point into fast map space.
        Returns coordinates of point in the space.
        """
        x = _Point(point, np.zeros(len(self.pivots), float))
        for k in range(len(self.pivots)):
            x.coords[k] = self.x_p(x, k)
        return x.coords

# Copyright (c) 2009, Gunnar Aastrand Grimnes
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#     * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#     * Neither the name of the <ORGANIZATION> nor the names of its
#     contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
